/***************************************************************************
 stdafx.h  -  include file for standard system include files, or project
              specific include files that are used frequently, but are
              changed infrequently.
*/

#if !defined(__stdafx_h)
#define __stdafx_h

#include <tchar.h>
#include <stdio.h>
#include <io.h>

#include <string>
#include <vector>
#include <list>

#endif
